using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ResolutionManager : MonoBehaviour
{

    public Dropdown resolutionDropdown;
    public Dropdown qualityDropdown;

    public Toggle fullscreenToggle;

    private List<string> resolutions;

    private void Awake()
    {
        // Build resolutions
        resolutionDropdown.ClearOptions();
        resolutions = new List<string>();
        for (int index = 0; index < Screen.resolutions.Length; index++)
        {
            resolutions.Add(string.Format("{0} x {1}", Screen.resolutions[index].width, Screen.resolutions[index].height));
        }
        resolutionDropdown.AddOptions(resolutions);
    }

    //Set the resolution to the values of the dropdown menu. 
    //Also used for fullscreen mode by accessing the fullscreen toggle
    public void SetResolution()
    {
        Screen.SetResolution(Screen.resolutions[resolutionDropdown.value].width, Screen.resolutions[resolutionDropdown.value].height, fullscreenToggle.isOn);
    }

    //Sets the quality to the index within the engine dropdown
    public void SetQualityLevel(int qualityIndex)
    {
        QualitySettings.SetQualityLevel(qualityIndex);
    }

    //Sets the values on the menus to match the engine
    private void OnEnable()
    {
        fullscreenToggle.isOn = Screen.fullScreen;
        qualityDropdown.value = QualitySettings.GetQualityLevel();
    }

}

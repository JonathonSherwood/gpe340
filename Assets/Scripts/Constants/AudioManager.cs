using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class AudioManager : MonoBehaviour
{
    //public GameObject soundClip;

    [System.Serializable]
    public class NamedSound
    {
        public AudioClip clip;
        public string name;
        public AudioMixerGroup output;
    }

    public NamedSound[] sounds;


    /// <summary>
    /// Used similarly to PlayClipAtPoint but with AudioMixer support
    /// </summary>
    /// <param name="name"></param>
    /// <param name="location"></param>
    public void PlaySoundAtLocation(string name, Vector3 location)
    {
        //Create a blank game object with an audiosource attached
        GameObject soundobject = new GameObject("soundClip");
        soundobject.transform.position = location;
        soundobject.AddComponent<AudioSource>();
        AudioSource audioSource = soundobject.GetComponent<AudioSource>();
        audioSource.spatialBlend = 1;

        foreach (NamedSound sound in sounds)
        {
            if (sound.name == name)
            {
                //attach the proper clip and audiomixer to the clip
                audioSource.clip = sound.clip;
                audioSource.outputAudioMixerGroup = sound.output;

                //then play it
                audioSource.Play();

                //then destroy it after it completes the length of the audio
                Destroy(soundobject, sound.clip.length);
            }
        }
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenu : MonoBehaviour
{
    public GameObject mainMenu, healthbar, staminabar, livesremaining, weaponImage;

    // Start is called before the first frame update
    void Start()
    {
        GameManager.instance.isPaused = true;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void PlayGame()
    {
        //Prevents gameplay UI from interfering with main menu
        mainMenu.SetActive(false);
        healthbar.SetActive(true);
        staminabar.SetActive(true);
        livesremaining.SetActive(true);
        weaponImage.SetActive(true);
        GameManager.instance.isPaused = false;
    }

}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;

public class SliderManager : MonoBehaviour
{
    public Slider slider;
    public string parameterToMatch;
    public AudioMixer audioMixer;


    public void UpdateMixerValue()
    {
        audioMixer.SetFloat(parameterToMatch, slider.value);

    }

}

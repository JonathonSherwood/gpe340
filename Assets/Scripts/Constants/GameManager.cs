using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

//This class represents the game, everything should be accessable from here!
public class GameManager : MonoBehaviour
{

    public static GameManager instance;

    [Header("Managers")]
    //public MenuManager menuManager;
    //public AudioManager audioManager;

    [Header("Gameplay Stats")]
    public float maxPlayerLives = 3;
    [HideInInspector]public float currentPlayerLives;
    public GameObject player;
    public float respawnTimer = 3;
    private float respawnTimeRemaining = 0;

    [HideInInspector]public PlayerController playerController;
    private UICanvas canvas;
    private bool isGameOver = false;
    [HideInInspector] public AudioManager audiomanager;

    [HideInInspector]public bool isPaused = false;



    // Start is called before the first frame update
    void Awake()
    {
        //If I'm the first
        if(instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            //There is already a gamemanager so destroy this
            Destroy(gameObject);
        }
    }

    private void Start()
    {
        currentPlayerLives = maxPlayerLives;
    }

    // Update is called once per frame
    void Update()
    {
        if (canvas == null)
        {
            canvas = FindObjectOfType<UICanvas>();
        }

        if (audiomanager == null)
        {
            audiomanager = FindObjectOfType<AudioManager>();
        }

        if (playerController == null && currentPlayerLives <= 0 && !isGameOver)
        {
            isGameOver = true;
        }

        if (playerController == null && currentPlayerLives > 0)
        {
            SpawnPlayer();
        }

        print(currentPlayerLives);
        HandleGameOver();
    }

    public void ReloadPlayScene()
    {

        Time.timeScale = 1;
        SceneManager.LoadScene(0);
        currentPlayerLives = maxPlayerLives + 1;
        respawnTimeRemaining = 0;
        isGameOver = false;
        isPaused = false;
    }

    public void SpawnPlayer()
    {
        //Pauses the game during countdown that is displayed to the player
        isPaused = true;
        canvas.respawnCountdown.enabled = true;
        canvas.darkOverlay.enabled = true;
        respawnTimeRemaining -= Time.deltaTime;
        if (respawnTimeRemaining >= 0.5f)
        {
            canvas.respawnCountdown.text = respawnTimeRemaining.ToString("F0");
        } else
        {
            canvas.respawnCountdown.text = "Go!";
        }

        //Respawns the player and turns the game back on
        if (respawnTimeRemaining <= 0)
        {
            canvas.darkOverlay.enabled = false;
            Vector3 playerSpawn = GameObject.Find("PlayerSpawn").transform.position;
            playerController = Instantiate(player, playerSpawn, Quaternion.identity).GetComponent<PlayerController>();
            currentPlayerLives--;
            respawnTimeRemaining = respawnTimer;
            isPaused = false;
            canvas.respawnCountdown.enabled = false;
            canvas.darkOverlay.enabled = false;
        }
    }

    public void HandleGameOver()
    {
        if (isGameOver)
        {
            isPaused = true;
            Time.timeScale = 0;
            canvas.darkOverlay.enabled = true;
            canvas.gameOverMenu.SetActive(true);

        }
    }
    
}

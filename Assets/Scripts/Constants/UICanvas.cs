using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UICanvas : MonoBehaviour
{
    public Image staminaBar;
    public Image healthBar;
    public Image weaponImage;
    public Image darkOverlay;
    public GameObject gameOverMenu;
    public Text livesRemaining;
    public Text respawnCountdown;
    private Health health;
    private Pawn pawn;

    // Update is called once per frame
    void LateUpdate()
    {
        if (GameManager.instance.isPaused == true) return;
        //Sets new player whenever an old one is missing
        if(pawn == null && GameManager.instance.playerController != null)
        {
            pawn = FindObjectOfType<PlayerController>().gameObject.GetComponent<Pawn>();
            staminaBar.fillAmount = pawn.staminaRemaining / pawn.maxStamina;
            health = FindObjectOfType<PlayerController>().gameObject.GetComponent<Health>();
            healthBar.fillAmount = health.currentHealth / health.maxHealth;
        }

        UpdateHealthBar();
        UpdateStaminaBar();
        UpdateWeaponImage();
        UpdateLivesRemainingText();
    }

    public void UpdateHealthBar()
    {
        healthBar.fillAmount = health.currentHealth / health.maxHealth; //Fills the health bar canvas relative ito remaining health
    }

    public void UpdateStaminaBar()
    {
        staminaBar.fillAmount = pawn.staminaRemaining / pawn.maxStamina; //Fills the stamina bar canvas relative ito remaining stamina
    }

    public void UpdateWeaponImage()
    {
        //Makes the weapon sprite transparent if there is no weapon
        if (pawn.weapon == null)
        {
            weaponImage.color = new Color(0, 0, 0, 0);
            return;
        }
        else
        {
            weaponImage.sprite = pawn.weapon.weaponImage;
            weaponImage.color = new Color(255, 255, 255, 255);
        }
        
    }

    public void UpdateLivesRemainingText()
    {
        livesRemaining.text = "Lives: " + GameManager.instance.currentPlayerLives;
    }

}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MenuManager : MonoBehaviour
{
    public GameObject uiCanvas;
    public GameObject pauseCanvas;
    public GameObject settingsCanvas;
    

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape) && GameManager.instance.isPaused == false)
        {
            PressPause();
        }
    }

    public void PressPause()
    {
        uiCanvas.GetComponent<UICanvas>().darkOverlay.enabled = true;
        Time.timeScale = 0;
        GameManager.instance.isPaused = true;
        pauseCanvas.gameObject.SetActive(true);
    }

    public void EnterSettings()
    {
        pauseCanvas.gameObject.SetActive(false);
        settingsCanvas.gameObject.SetActive(true);
    }

    public void ExitSettings()
    {
        pauseCanvas.gameObject.SetActive(true);
        settingsCanvas.gameObject.SetActive(false);
    }

    public void ExitMenu()
    {
        pauseCanvas.gameObject.SetActive(false);
        settingsCanvas.gameObject.SetActive(false);
        GameManager.instance.isPaused = false;
        uiCanvas.GetComponent<UICanvas>().darkOverlay.enabled = false;
        Time.timeScale = 1;
    }


}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DummyPointsOnKill : MonoBehaviour
{
    public float pointValue = 0;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void AddToScore()
    {
        Debug.Log("You gained " + pointValue + " points");
    }

}

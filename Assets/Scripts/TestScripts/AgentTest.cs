using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class AgentTest : MonoBehaviour
{
    private NavMeshAgent agent;
    public Transform targetTransform;
    private Animator anim;
    private Pawn pawn;
    private float timeForNextNavigationCheck;
    private float timeBetweenNavigationChecks = 0.5f;

    // Start is called before the first frame update
    void Start()
    {
        //Set timer
        timeForNextNavigationCheck = Time.time + timeBetweenNavigationChecks;

        agent = GetComponent<NavMeshAgent>();
        anim = GetComponent<Animator>();
        pawn = GetComponent<Pawn>();
    }

    // Update is called once per frame
    void Update()
    {
        //Prevents target destination from being calculated per frame draw
        if (Time.time >= timeForNextNavigationCheck)
        {
            //Do check
            agent.SetDestination(targetTransform.position);
            //Reset timer
            timeForNextNavigationCheck = Time.time + timeBetweenNavigationChecks;
        }

        //Get the target velocity of the navmesh agent
        Vector3 desiredVelocity = agent.desiredVelocity;
        //Set this to local direction
        desiredVelocity = transform.InverseTransformDirection(desiredVelocity);

        //Use this to change the pawns speed to match player's
        desiredVelocity = desiredVelocity.normalized * pawn.moveSpeed;

        //pass this to the animator
        anim.SetFloat("Forward", desiredVelocity.z);
        anim.SetFloat("Right", desiredVelocity.x);

        //Then rotate towards movement direction
        Quaternion rotationToMovementDirection = Quaternion.LookRotation(agent.desiredVelocity, Vector3.up);

        transform.rotation = Quaternion.RotateTowards(transform.rotation, rotationToMovementDirection, pawn.rotateSpeed * Time.deltaTime);

        print(agent.desiredVelocity.z);
        print(agent.desiredVelocity.x);
    }

    private void OnAnimatorMove()
    {
        //Runs every time the animator tells us to move
        //Tell the navmesh agent we have moved so it doesn't have to move us
        agent.velocity = anim.velocity;
    }

}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Health : MonoBehaviour
{
    [Header("Values")]
    public float maxHealth;
    public float currentHealth;
    [Header("Events")]
    public UnityEvent OnTakeDamage;
    public UnityEvent OnDie;

    private Pawn pawn;

    public float despawnTimer;
    private float currentDespawnTimer;

    private DropSpawn dropSpawn;

    public string damagedSound;
    public string deathSound;


    // Start is called before the first frame update
    void Start()
    {
        currentDespawnTimer = despawnTimer;
        pawn = GetComponent<Pawn>();

        //Prevents looking for a dropspawn if this creature isn't designed to drop anythin
        if(GetComponent<DropSpawn>() != null)
        {
            dropSpawn = GetComponent<DropSpawn>();
        }
    }

    // Update is called once per frame
    void Update()
    {


        //Character dies at 0 health
        if(currentHealth <= 0 )
        {
            OnDie.Invoke();
        }
    }

    public void DestroyObject()
    {
        Destroy(gameObject);
    }

    //Called to remove AI on death so that the spawner is cleared
    public void Despawn()
    {
        currentDespawnTimer -= Time.deltaTime;
        if(currentDespawnTimer <= 0)
        {
            Destroy(gameObject);
        }
    }

    //Called to remove weapon on death so that the ragdoll isn't holding it
    public void RemoveWeapon()
    {
        if(pawn.weapon != null)
        {
            Destroy(pawn.weapon.gameObject);
        }
    }

    public void DropItem()
    {
        Instantiate(dropSpawn.ChooseSpawnObject(), transform.position, Quaternion.identity);
    }

    public void TakeDamage(float amountofDamage)
    {
        GameManager.instance.audiomanager.PlaySoundAtLocation(damagedSound, transform.position);

        //Call On Take Damage Event
        OnTakeDamage.Invoke();

        currentHealth -= amountofDamage;
        //If health is less than 0 you die
        if (currentHealth <= 0)
        {
            GameManager.instance.audiomanager.PlaySoundAtLocation(deathSound, transform.position);
            OnDie.Invoke();
        }
        else
        {
            //Don't go over max health
            currentHealth = Mathf.Min(currentHealth, maxHealth);
        }
    }

}

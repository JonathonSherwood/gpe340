using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public abstract class Pickup : MonoBehaviour
{
    public UnityEvent OnSpawn;
    public UnityEvent OnPickup;

    [Header("Animation")]
    public float rotationSpeed;


    // Start is called before the first frame update
    public virtual void Start()
    {
        OnSpawn.Invoke();
    }

    // Update is called once per frame
    public virtual void Update()
    {
        //Spins pickups for visual effect
        transform.Rotate(0, rotationSpeed * Time.deltaTime, 0 * Time.deltaTime);
    }

    public virtual void OnTriggerEnter(Collider other)
    {
        Pawn otherpawn = other.GetComponent<Pawn>();
        if(otherpawn != null)
        {
            OnPickup.Invoke();
            Destroy(gameObject);
        }
    }

}

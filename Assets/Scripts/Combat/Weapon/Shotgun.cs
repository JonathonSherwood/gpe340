using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shotgun : Weapon
{
    public Transform firepoint2;
    public Transform firepoint3;

    private Transform[] firepoints;

    // Start is called before the first frame update
    public override void Start()
    {
        //Calls the base class
        base.Start();

        firepoints = new Transform[] { firePoint, firepoint2, firepoint3};
        print(firepoints.Length);
    }

    // Update is called once per frame
    public override void Update()
    {
        //Calls the base class 
        base.Update();
    }

    public void ShootShotgunShells()
    {
        foreach (Transform fp in firepoints)
        {
            //instantiates a bullet at the fire location 
            GameObject projectile = Instantiate(projectilePrefab, fp.position, fp.rotation) as GameObject;
            Projectile projectileScript = projectile.GetComponent<Projectile>();

            //Transfer information to the bullet 
            if (projectile != null)
            {
                projectileScript.damageDone = damageDone;
                projectileScript.moveSpeed = weaponSpeed;
                projectileScript.lifeSpan = weaponDuration;
            }

            //Bullet will handle the rest
        }
    }


}

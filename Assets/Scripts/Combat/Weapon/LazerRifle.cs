using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LazerRifle : Weapon
{
    // Start is called before the first frame update
    public override void Start()
    {
        //Calls the base class
        base.Start();
    }

    // Update is called once per frame
    public override void Update()
    {
        //Calls the base class 
        base.Update();
    }

    public void ShootPlasmaSphere()
    {
        //instantiates a plasma sphere at the fire location 
        GameObject projectile = Instantiate(projectilePrefab, firePoint.position, firePoint.rotation) as GameObject;
        Projectile projectileScript = projectile.GetComponent<Projectile>();

        //Transfer information to the sphere 
        if(projectile != null)
        {
            projectileScript.damageDone = damageDone;
            projectileScript.moveSpeed = weaponSpeed;
            projectileScript.lifeSpan = weaponDuration;
        }

        //Sphere will handle the rest
    }

}

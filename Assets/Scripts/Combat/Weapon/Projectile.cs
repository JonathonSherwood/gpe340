using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    //Data will be set on instantiation by instantiating object
    [HideInInspector]public float damageDone;
    [HideInInspector]public float moveSpeed;
    [HideInInspector]public float lifeSpan;
    private Rigidbody rb;

    private bool isColliding = false;

    public string shootSound;
    public GameObject explosionParticles;

    public virtual void Awake()
    {
        rb = GetComponent<Rigidbody>();
    }

    // Start is called before the first frame update
    void Start()
    {
        GameManager.instance.audiomanager.PlaySoundAtLocation(shootSound, transform.position);
        Destroy(gameObject, lifeSpan);

    }

    // Update is called once per frame
    public virtual void Update()
    {
        //Launching projectile forward
        rb.MovePosition(transform.position + (transform.forward * moveSpeed * Time.deltaTime));
    }

    private void OnTriggerEnter(Collider other)
    {
        if (isColliding) return;
        isColliding = true;

        Health otherHealth = other.GetComponent<Health>();

        //Check to see if the other object has health
        if(otherHealth != null)
        {
            otherHealth.TakeDamage(damageDone);
        }
        print(other.name);
        //Prevents bullets from destroying other bullets
        if (other.tag != "Projectile")
        {
            //GameManager.instance.audiomanager.PlaySoundByName("Impact", transform.position);
            GameManager.instance.audiomanager.PlaySoundAtLocation("Impact", transform.position);
            GameObject explosion = Instantiate(explosionParticles, transform.position, Quaternion.identity);
            Destroy(explosion, 1);
            Destroy(gameObject);
        }
    }
}

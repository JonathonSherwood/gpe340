using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Weapon : MonoBehaviour
{
    [Header("Events")]
    public UnityEvent OnShoot;
    public UnityEvent OnPullTrigger;
    public UnityEvent OnReleaseTrigger;
    public UnityEvent OnAlternateAttackStart;
    public UnityEvent OnAlternateAttackEnd;

    [HideInInspector] public bool isAutoFiring; 

    [Header("Data")]
    public float fireDelay; //Seconds between shots
    [HideInInspector]public float countdown;
    public float damageDone;
    public float weaponSpeed;
    public float weaponDuration;

    [Header("Variables")]
    public Transform firePoint;
    public GameObject projectilePrefab;
    public Sprite weaponImage;
    public GameObject muzzleFlashPoint;
    public GameObject muzzleFlashParticles;

    [Header("Hand Points")]
    public Transform rightHandPoint;
    public Transform leftHandPoint;

    public virtual void Start()
    {
        countdown = fireDelay;
    }

    public virtual void Update()
    {
        //Subtract the time it took to play the last frame from our cooldown
        countdown -= Time.deltaTime;

        if (isAutoFiring && countdown <= 0)
        {
            //shoot
            Shoot();
            //reset timer
            countdown = fireDelay;
        }
    }

    public void Shoot()
    {
        OnShoot.Invoke();
        muzzleFlashParticles.GetComponent<ParticleSystem>().Play();
    }

    public void StartAutofire()
    {
        isAutoFiring = true;
    }

    public void EndAutoFire()
    {
        isAutoFiring = false;
    }

    public void ToggleAutoFire()
    {
        isAutoFiring = !isAutoFiring;
    }

}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rocket : Projectile
{
    private Camera playerCamera;
    public float rotationSpeed;

    // Start is called before the first frame update
    void Start()
    {
        GameManager.instance.audiomanager.PlaySoundAtLocation(shootSound, transform.position);
        playerCamera = GameObject.Find("Main Camera").GetComponent<Camera>();
    }

    // Update is called once per frame
    public override void Update()
    {
        RotateToMouse();
        Destroy(gameObject, 3f);
        base.Update();
    }

    /// <summary>
    /// Allows the rocket to chase the player's mouse
    /// </summary>
    void RotateToMouse()
    {
        //Create a plane object to represent all points in 2d
        Plane groundPlane;

        //Set that plane so it is the x,z the player is standing on
        groundPlane = new Plane(Vector3.up, transform.position);

        //Cast a ray from our camera toward the plane, through the mouse cursor
        float distance;
        Ray cameraRay = playerCamera.ScreenPointToRay(Input.mousePosition);
        groundPlane.Raycast(cameraRay, out distance);

        //Find where that ray hits the plane
        Vector3 raycastPoint = cameraRay.GetPoint(distance);

        //Rotate player towards that point
        RotateTowards(raycastPoint);

    }

    /// <summary>
    /// Rotates towards player's mouse
    /// </summary>
    /// <param name="targetPoint"></param>
    public void RotateTowards(Vector3 targetPoint)
    {
        //Find rotation for the target point
        //Find Vector to point
        Vector3 targetVector = targetPoint - transform.position;

        //Find rotation down that vector
        Quaternion targetRotation = Quaternion.LookRotation(targetVector, Vector3.up);

        //Change rotation slowly to target location
        transform.rotation = Quaternion.RotateTowards(transform.rotation, targetRotation, rotationSpeed * Time.deltaTime);
    }

}

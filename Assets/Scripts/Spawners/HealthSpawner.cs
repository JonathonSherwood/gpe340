using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthSpawner : Pickup
{
    public float healAmount;


    public override void OnTriggerEnter(Collider other)
    {

        Health otherHealth = other.GetComponent<Health>();

        //Check to see if the other object has health
        if (otherHealth != null)
        {
            otherHealth.TakeDamage(-healAmount);
        }
        Destroy(gameObject);
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    public float respawnTimer;
    public GameObject objectToSpawn;
    private GameObject spawnedObject;
    private float respawnCountdown;
    public Color setColor;
    private Mesh spawnMesh;


    // Start is called before the first frame update
    void Start()
    {
        //Sets countdown timer to max on start
        respawnCountdown = respawnTimer;
        //Spawns object on start
        spawnedObject = Instantiate(objectToSpawn, transform.position, transform.rotation);

        
    }

    // Update is called once per frame
    void Update()
    {
        //If the spawned objet is missing, count down
        if(spawnedObject == null)
        {
            respawnCountdown -= Time.deltaTime;
        } else //Otherwise keep timer paused
        {
            respawnCountdown = respawnTimer;
        }

        //Once countdown reaches zero, spawn object
        if(respawnCountdown <=0)
        {
            spawnedObject = Instantiate(objectToSpawn, transform.position, transform.rotation);
        }
    }

    private void OnDrawGizmos()
    {
        //Checks for what type of mesh the individual spawned object has
        //This corrects for objects that have children, skinned meshes, or if the base has a mesh
        if (objectToSpawn.GetComponentInChildren<SkinnedMeshRenderer>() != null)
        {
            spawnMesh = objectToSpawn.GetComponentInChildren<SkinnedMeshRenderer>().sharedMesh;
        }

        if(objectToSpawn.GetComponent<MeshFilter>() != null)
        {
            spawnMesh = objectToSpawn.GetComponent<MeshFilter>().sharedMesh;
        }

        if(objectToSpawn.GetComponentInChildren<MeshFilter>() != null)
        {
            spawnMesh = objectToSpawn.GetComponentInChildren<MeshFilter>().sharedMesh;
        }

        //Sets the matrix to the current transforms world space
        Matrix4x4 myNewCoordinateSystem = Matrix4x4.TRS(transform.position, transform.rotation, Vector3.one);
        //Allows the designer to set the color
        Gizmos.color = setColor; 

        Gizmos.matrix = myNewCoordinateSystem;

        //Draws the mesh related to the spawned object
        Gizmos.DrawMesh(spawnMesh, Vector3.zero, Quaternion.identity);

        //Displays a ray for the designer to see what is 'forward'
        Gizmos.DrawRay(Vector3.zero, Vector3.forward);
        
    }
}

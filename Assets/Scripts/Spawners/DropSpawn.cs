using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DropSpawn : MonoBehaviour
{
    public RandomWeightedObject[] objectsToSpawn;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.I))
        {
            Instantiate(ChooseSpawnObject());
        }
    }

    public GameObject ChooseSpawnObject()
    {
        //Var to hold spawn object
        GameObject objectToSpawn;

        //Create second parallel array - holds cutoffs where it changes to the next item on the list
        //                                          Anything below cutoff is the parallel weighted object
        float[] CDFArray = new float[objectsToSpawn.Length];

        //var to hold cumulative density (total of weights)
        float cumulativeDenisty = 0;

        //Fill CDF Array
        for (int i=0; i< objectsToSpawn.Length; i++)
        {
            //Add this objects weight so we know cutoff
            cumulativeDenisty += objectsToSpawn[i].weight;
            //store into the CDFArray
            CDFArray[i] = cumulativeDenisty;
        }

        //Choose a random number up to the max cutoff
        float rand = Random.Range(0.0f, cumulativeDenisty);

        /* Linear search
        //Look through my CDF to find where random number would fall -- which CDF Index would it fall under
        for (int i = 0; i < CDFArray.Length; i++)
        {
            if(rand < CDFArray[i])
            {
                objectToSpawn = objectsToSpawn[Random.Range(0, objectsToSpawn.Length)].objectToSpawn;
                return objectToSpawn;
            }
        }
        */

        int selectedIndex = System.Array.BinarySearch(CDFArray, rand);

        //If the index is negative
        if(selectedIndex < 0)
        {
            //It's not the value we have to FLIP the value to find the index we want
            selectedIndex = ~selectedIndex; 
        }

        objectToSpawn = objectsToSpawn[selectedIndex].objectToSpawn;
        return objectToSpawn;

    }

}

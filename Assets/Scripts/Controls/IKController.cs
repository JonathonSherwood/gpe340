using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IKController : MonoBehaviour
{
    [HideInInspector]public Transform rightHandPoint;
    [HideInInspector]public Transform leftHandPoint;

    private Animator anim;
    private Pawn pawn;

    private void Awake()
    {
        anim = GetComponent<Animator>();
        pawn = GetComponent<Pawn>();
    }

    public void OnAnimatorIK(int layerIndex)
    {
        //check that we have a weapon - no weapon no IK
        if (pawn.weapon == null)
        {
            anim.SetIKPositionWeight(AvatarIKGoal.RightHand, 0.0f);
            anim.SetIKPositionWeight(AvatarIKGoal.LeftHand, 0.0f);
            anim.SetIKRotationWeight(AvatarIKGoal.RightHand, 0.0f);
            anim.SetIKRotationWeight(AvatarIKGoal.LeftHand, 0.0f);

            return;
        }

        //Get points from right and left hand on weapon
        if(pawn.weapon.rightHandPoint == null)
        {
            anim.SetIKPositionWeight(AvatarIKGoal.RightHand, 0.0f);
            anim.SetIKRotationWeight(AvatarIKGoal.RightHand, 0.0f);
        }
        else
        {
            anim.SetIKPosition(AvatarIKGoal.RightHand, pawn.weapon.rightHandPoint.position);
            anim.SetIKRotation(AvatarIKGoal.RightHand, pawn.weapon.rightHandPoint.rotation);
            anim.SetIKPositionWeight(AvatarIKGoal.RightHand, 1);
            anim.SetIKRotationWeight(AvatarIKGoal.RightHand, 1);
        }

        if(pawn.weapon.leftHandPoint == null)
        {
            anim.SetIKPositionWeight(AvatarIKGoal.LeftHand, 0.0f);
            anim.SetIKRotationWeight(AvatarIKGoal.LeftHand, 0.0f);
        }
        else
        {
            anim.SetIKPosition(AvatarIKGoal.LeftHand, pawn.weapon.leftHandPoint.position);
            anim.SetIKRotation(AvatarIKGoal.LeftHand, pawn.weapon.leftHandPoint.rotation);
            anim.SetIKPositionWeight(AvatarIKGoal.LeftHand, 1);
            anim.SetIKRotationWeight(AvatarIKGoal.LeftHand, 1);
        }


    }
}

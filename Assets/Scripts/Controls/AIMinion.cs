using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class AIMinion : AIController
{
    private Pawn pawn;
    private NavMeshAgent agent;

    private PlayerController player;

    public float maxShootingError = 0.1f;
    public float minShootDistance;
    public float maxShootDistance;

    private float zeroLeadDistance;
    private float maxLeadDistance = 30;

    private Vector3 leadVector; //How far in front of (or otherwise awy from) player to shoot
    public float leadModifier = 1.0f;

    public float fireDelay;
    private float timeLeftTillFire;

    private float timeForNextNavigationCheck;
    private float timeBetweenNavigationChecks = 0.5f;

    public bool stopShootingStupid;

    public override void Awake()
    {
        pawn = GetComponent<Pawn>();
        
        agent = GetComponent<NavMeshAgent>();
    }


    // Start is called before the first frame update
    public override void Start()
    {

        if(player == null) FindPlayer();
    }

    // Update is called once per frame
    public override void Update()
    {
        if (GameManager.instance.isPaused == true) return;

        if (player == null)
        {
            FindPlayer();
        }
        else
        {

            SetLeadVector();
            MoveToPlayer();
        }

        if(pawn.weapon != null && pawn.weapon.fireDelay > 0)
        {
            if (!stopShootingStupid)
            {
                ShootAtPlayer();
            }
        }

    }

    public void SetLeadVector()
    {
        //find distance to player
        float distanceToPlayer = Vector3.Distance(player.transform.position, pawn.transform.position);

        //Clamp the distance between 0 and max lead
        distanceToPlayer = Mathf.Clamp(distanceToPlayer, zeroLeadDistance, maxLeadDistance);

        float dTPFromMin = distanceToPlayer - zeroLeadDistance;
        float totalLeadDistanceRange = maxLeadDistance - zeroLeadDistance;

        //Find what percentage of the total range I'm currently at
        float percentOfLeadDistance = dTPFromMin / totalLeadDistanceRange;

        //Now that we have range, multiply the lead by it
        //Find a few "Seconds" in front of the player
        leadVector = player.pawn.anim.velocity * (leadModifier * percentOfLeadDistance);
    }

    public void ShootAtPlayer()
    {
        if(player == null) return;
        float distanceToPlayer = Vector3.Distance(player.transform.position, pawn.transform.position);

        //Prevents firing at the player so long as the ai is too close or too far away
        if (distanceToPlayer > minShootDistance && distanceToPlayer < maxShootDistance)
        {

            //Adds a random error margin for shooting at player
            float shootingError = Random.Range(-maxShootingError * 0.5f, maxShootingError * 0.5f);
            //Rotates off of the margin
            pawn.weapon.transform.Rotate(0, shootingError, 0);
            //Shoots
            if (!pawn.weapon.isAutoFiring)
            {
                pawn.weapon.OnPullTrigger.Invoke();
            }
            else
            {
                //Prevents target destination from being calculated per frame draw
                if (Time.time >= timeLeftTillFire)
                {
                    //Do check
                    pawn.weapon.Shoot();
                    //Reset timer
                    timeLeftTillFire = Time.time + fireDelay;
                }
            }
            //RotatesBack
            pawn.weapon.transform.Rotate(0, -shootingError, 0);
        }
    }

    public void FindPlayer()
    {
        player = GameObject.FindObjectOfType<PlayerController>();
    }

    public void RotateTowardsPlayer()
    {
        //find vector to player
        Vector3 vectorToPlayer = (player.pawn.transform.position + leadVector) - pawn.transform.position;

        //find quaternion that is looking down that vector
        Quaternion lookRotation = Quaternion.LookRotation(vectorToPlayer, Vector3.up);
        //rotate a little towards
        pawn.transform.rotation = Quaternion.RotateTowards(pawn.transform.rotation, lookRotation, pawn.rotateSpeed * Time.deltaTime);
    }

    public void MoveToPlayer()
    {
        //Prevents target destination from being calculated per frame draw
        if (Time.time >= timeForNextNavigationCheck)
        {
            //Do check
            agent.SetDestination(player.pawn.transform.position);
            //Reset timer
            timeForNextNavigationCheck = Time.time + timeBetweenNavigationChecks;
        }

        //Find the vector of the path
        Vector3 desiredMovement = agent.desiredVelocity;

        //Adjust to pawn speed
        desiredMovement = desiredMovement.normalized * pawn.moveSpeed;

        //Set to local direction
        desiredMovement = pawn.transform.InverseTransformDirection(desiredMovement);

        //Send to animator
        pawn.anim.SetFloat("Forward", desiredMovement.z);
        pawn.anim.SetFloat("Right", desiredMovement.x);

        RotateTowardsPlayer();
    }

    public void OnAnimatorMove()
    {
        //Tell agent that animator moved us so it doesn't have to
        agent.velocity = pawn.anim.velocity;
    }

}



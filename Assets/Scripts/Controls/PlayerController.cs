using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{
    public Pawn pawn;
    public Camera playerCamera;
    public float cameraFollowSpeed; 
    private bool attemptingToSprint = false;

    private Health health;

    private Vector3 target;


    // Start is called before the first frame update
    void Start()
    {
        //make sure the public assets are loaded
        if (playerCamera == null) Debug.LogWarning("ERROR: No Camera Assigned To PlayerCamera");
        if (pawn == null) Debug.LogWarning("ERROR: No Pawn Assigned");

        health = GetComponent<Health>();
        health.currentHealth = health.maxHealth;

    }

    // Update is called once per frame
    void Update()
    {
        if (GameManager.instance.isPaused) return;

        if(playerCamera == null)
        {
            playerCamera = GameObject.Find("Main Camera").GetComponent<Camera>();
        }
        MovementInput();
        RotateToMouse();
        Sprint();
        MoveCameraToPawn();

    }

    void MoveCameraToPawn()
    {
        //Set the target position to be the pawns 2d location, while leaving the camera where it is
        target = new Vector3(pawn.transform.position.x, 15, pawn.transform.position.z);

        //Slowly move the camera towards the target
        playerCamera.transform.position = Vector3.MoveTowards(playerCamera.transform.position, target, cameraFollowSpeed * Time.deltaTime);
    }

    void MovementInput()
    {
        //create move vector based on inputs
        Vector3 moveVector = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));

        //Adjust move vector to be the direction the character is facing
        // Find out what local forward/right is compared to north/south
        moveVector = pawn.transform.InverseTransformDirection(moveVector);


        //Prevent keyboard input from going above 1.0f
        moveVector = Vector3.ClampMagnitude(moveVector, 1.0f);
        //Send clamped vector to pawn
        pawn.Move(moveVector);

        //Read Fire Button Inputs
        GetButtonInputs();
    }

    private void GetButtonInputs()
    {
        if (pawn.weapon == null) return;


        if(Input.GetButtonDown("Fire1") && pawn.weapon.fireDelay > 0)
        {
            pawn.weapon.OnPullTrigger.Invoke();
        }
        if (Input.GetButtonUp("Fire1"))
        {
            pawn.weapon.OnReleaseTrigger.Invoke();
        }
        if (Input.GetButtonDown("Fire2"))
        {
            pawn.weapon.OnAlternateAttackStart.Invoke();
        }
        if (Input.GetButtonUp("Fire2"))
        {
            pawn.weapon.OnAlternateAttackEnd.Invoke();
        }
    }

    public void Sprint()
    {
        //Allows for AI to automatically set trying to sprint, or have the player attempt it on input
        attemptingToSprint = ((Input.GetKey(KeyCode.LeftShift) && Input.GetAxis("Vertical") != 0.0f) ? true : false);


        //If the player is trying to sprint...
        if(attemptingToSprint == true)
        { 
            //Drain remaining stamina
            if (pawn.staminaRemaining > 0)
                pawn.staminaRemaining = pawn.staminaRemaining - pawn.staminaDepletionRate * Time.deltaTime;

            //If the player has stamina remaining...
            if (pawn.staminaRemaining > 0)
            {
                //Increase movement speed and drain stamina
                pawn.currentSpeed = pawn.sprintSpeed;
            }
            //If the player doesn't have stamina...
            else
            {
                //But they are still holding sprint, move slower than normal
                pawn.currentSpeed = pawn.moveSpeed * 0.25f;
            }
        }
        //While not trying to sprint
        else
        {
            //Return speed to normal
            pawn.currentSpeed = pawn.moveSpeed;

            //Recharge stamina to max
            if (pawn.staminaRemaining < pawn.maxStamina)
                pawn.staminaRemaining = pawn.staminaRemaining + pawn.staminaRegenRate * Time.deltaTime;
        }
    }

    void RotateToMouse()
    {
        //Create a plane object to represent all points in 2d
        Plane groundPlane;

        //Set that plane so it is the x,z the player is standing on
        groundPlane = new Plane(Vector3.up, pawn.transform.position);

        //Cast a ray from our camera toward the plane, through the mouse cursor
        float distance;
        Ray cameraRay = playerCamera.ScreenPointToRay(Input.mousePosition);
        groundPlane.Raycast(cameraRay, out distance);

        //Find where that ray hits the plane
        Vector3 raycastPoint = cameraRay.GetPoint(distance);

        //Rotate player towards that point
        pawn.RotateTowards(raycastPoint);
    }
}

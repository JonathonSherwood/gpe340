using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pawn : MonoBehaviour
{
    [Header("Movement")]
    [Tooltip("Meters per second")]
    public float moveSpeed = 1; //Meters per second 
    [Tooltip("Meters per second")]
    public float sprintSpeed = 2; //Meters per second
    [HideInInspector]public float currentSpeed = 1; //Meters per second
    [Tooltip("Degrees per second")]
    public float rotateSpeed= 180; //Degrees per second

    [Header("Stamina"), Tooltip("In seconds")]
    public float maxStamina = 10; //How many seconds you can sprint
    [Tooltip("Per second")]
    public float staminaDepletionRate = 1; //Per second
    [Tooltip("Per second")]
    public float staminaRegenRate = 1; //Per second
    [HideInInspector]public float staminaRemaining; //Per second

    [Header("Components")]
    public Weapon weapon;
    [HideInInspector]public Animator anim;

    [Header("Transforms")]
    public Transform weaponMountPoint;

    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animator>();

        //Set the current stamina and health to max on start
        staminaRemaining = maxStamina;
    }

    public void UnEquipWeapon()
    {
        if (weapon != null)
        {
            //Destroy equipped weapon
            Destroy(weapon.gameObject);
            //Reset variable to null
            weapon = null;
        }
    }

    private void Update()
    {

    }

    public void EquipWeapon(GameObject weaponPrefabToEquip)
    {
        //Unequip original weapon
        UnEquipWeapon();

        //Instantiate weapon to equip
        GameObject newWeapon = Instantiate(weaponPrefabToEquip, weaponMountPoint.position, weaponMountPoint.rotation) as GameObject;

        //Set its transform to the parent
        newWeapon.transform.parent = weaponMountPoint;

        //Set this pawn so the new weapon is used by code
        weapon = newWeapon.GetComponent<Weapon>();

    }

    /// <summary>
    ///  Pass input from the controller into the pawn's animator for movement
    /// </summary>
    /// <param name="moveVector"></param>
    public void Move(Vector3 moveVector)
    {
        //Create a new vector impacted by speed. Allows us to keep original vector for other checks
        Vector3 appliedSpeed = moveVector * currentSpeed;

        //Send parameters into animator
        anim.SetFloat("Right", appliedSpeed.x);
        anim.SetFloat("Forward", appliedSpeed.z);
    }

    /// <summary>
    /// Passes in a targetpoint for the character to rotate towards
    /// </summary>
    /// <param name="targetPoint"></param>
    public void RotateTowards(Vector3 targetPoint)
    {
        //Find rotation for the target point
        //Find Vector to point
        Vector3 targetVector = targetPoint - transform.position;

        //Find rotation down that vector
        Quaternion targetRotation = Quaternion.LookRotation(targetVector, Vector3.up);

        //Change rotation slowly to target location
        transform.rotation = Quaternion.RotateTowards(transform.rotation, targetRotation, rotateSpeed * Time.deltaTime);
    }




}

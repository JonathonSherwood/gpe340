using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RagdollController : MonoBehaviour
{
    public bool isRagdoll = false;

    private Rigidbody rb;
    private Collider mainCollider;
    private Animator anim;
    private List<Rigidbody> ragdollRB;
    private List<Collider> ragdollColliders;
    private AIMinion ai;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        mainCollider = GetComponent<Collider>();
        anim = GetComponent<Animator>();

        ragdollColliders = new List<Collider>(GetComponentsInChildren<Collider>());
        ragdollRB = new List<Rigidbody>(GetComponentsInChildren<Rigidbody>());

        ragdollRB.Remove(rb);
        ragdollColliders.Remove(mainCollider);

        ai = GetComponent<AIMinion>();
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.P))
        {
            ToggleRagdoll();
        }
    }

    public void ToggleRagdoll()
    {
        if(isRagdoll)
        {
            DeactivateRagdoll();
            isRagdoll = false;
        }
        else
        {
            ActivateRagdoll();
            isRagdoll = true;
        }
    }

    public void ActivateRagdoll()
    {
        ai.enabled = false;

        //turn on ragdoll colliders and rigidbodies
        foreach(Collider collider in ragdollColliders)
        {
            collider.enabled = true;
        }
        foreach (Rigidbody rb in ragdollRB)
        {
            rb.isKinematic = false;
        }

        //Turn OFF main collider, animator, rigidbody
        mainCollider.enabled = false;
        anim.enabled = false;
        rb.isKinematic = true;

    }

    public void DeactivateRagdoll()
    {
        ai.enabled = true;

        //turn off ragdoll colliders and rigidbodies
        foreach (Collider collider in ragdollColliders)
        {
            collider.enabled = false;
        }
        foreach (Rigidbody rb in ragdollRB)
        {
            rb.isKinematic = true;
        }

        //Turn on main collider, animator, rigidbody
        mainCollider.enabled = true;
        anim.enabled = true;
        rb.isKinematic = false;
    }


}

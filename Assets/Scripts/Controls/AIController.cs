using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class AIController : MonoBehaviour
{
    public abstract void Awake();

    public abstract void Start();

    public abstract void Update();
}
